#include "danfe.h"
#include "ui_danfe.h"
#include <QPainter>

Danfe::Danfe(QTextEdit *parent) :
    QTextEdit(parent),
    ui(new Ui::Danfe)
{
    ui->setupUi(this);
    setReadOnly(true);
}

Danfe::~Danfe()
{
    delete ui;
}

void Danfe::paintEvent(QPaintEvent *a_event)
{
    QTextEdit::paintEvent(a_event);
    QPainter painter(viewport());
    QPen pen;
    pen.setColor(Qt::black);
    pen.setWidth(1);
    painter.setPen(pen);
    painter.setRenderHint(QPainter::Antialiasing, true);

    int margem = 3;
    float widthLayoutSpacing = 0.0f;
    float heightLayoutSpacing = 0.0f;

    /*
     * Bolco 1
     */
//    QRectF rectTitle(1.0, 1.0, 1000.0 + (margem * 3), 400.0);
    QRectF rectTitle(margem, margem, 500.0 + (margem * 3), 200.0);
    painter.drawRect(rectTitle);
    painter.drawText(rectTitle, Qt::AlignCenter, tr("Web Danfe"));

    heightLayoutSpacing = rectTitle.height() + margem;
//    QRectF rectTipoDoCTE(1.0, heightLayoutSpacing, 250.0, 100.0);
    QRectF rectTipoDoCTE(margem, heightLayoutSpacing, 125.0, 50.0);
    painter.drawRect(rectTipoDoCTE);
    painter.drawText(rectTipoDoCTE, Qt::AlignCenter, tr("Tipo do CTE"));

    widthLayoutSpacing += rectTipoDoCTE.width() + margem;
//    QRectF rectTipoDoServico(widthLayoutSpacing, heightLayoutSpacing, 250.0, 100.0);
    QRectF rectTipoDoServico(widthLayoutSpacing, heightLayoutSpacing, 125.0, 10.0);
    painter.drawRect(rectTipoDoServico);
    painter.drawText(rectTipoDoServico, Qt::AlignCenter, tr("Tipo do Serviço"));

    widthLayoutSpacing += rectTipoDoCTE.width() + margem;
//    QRectF rectTomadorDoServico(widthLayoutSpacing, heightLayoutSpacing, 250.0, 100.0);
    QRectF rectTomadorDoServico(widthLayoutSpacing, heightLayoutSpacing, 125.0, 10.0);
    painter.drawRect(rectTomadorDoServico);
    painter.drawText(rectTomadorDoServico, Qt::AlignCenter, tr("Tomador do Serviço"));

    widthLayoutSpacing += rectTomadorDoServico.width() + margem;
//    QRectF rectFormaDePagamento(widthLayoutSpacing, heightLayoutSpacing, 250.0, 100.0);
    QRectF rectFormaDePagamento(widthLayoutSpacing, heightLayoutSpacing, 125.0, 10.0);
    painter.drawRect(rectFormaDePagamento);
    painter.drawText(rectFormaDePagamento, Qt::AlignCenter, tr("Forma de Pagamento"));

    /*
     * Bloco 1.2
    */
    widthLayoutSpacing = margem + rectTitle.width() + margem;
    heightLayoutSpacing = rectTitle.width() + margem;

//    QRectF rectDacte(widthLayoutSpacing, 1.0, 500.0, 100.0);
    QRectF rectDacte(widthLayoutSpacing, margem, 250.0, 50.0);
    painter.drawRect(rectDacte);
    painter.drawText(rectDacte, Qt::AlignCenter, tr("DACTE"));

    widthLayoutSpacing += rectDacte.width() + margem;
//    QRectF rectModal(widthLayoutSpacing, 1.0, 450.0, 100.0);
    QRectF rectModal(widthLayoutSpacing, margem, 225.0, 50.0);
    painter.drawRect(rectModal);
    painter.drawText(rectModal, Qt::AlignCenter, tr("MODAL"));

    //Height update
    heightLayoutSpacing = rectDacte.height() + margem;
    widthLayoutSpacing = margem + rectTitle.width() + margem;
//    QRectF rectModelo(widthLayoutSpacing, heightLayoutSpacing, 100.0, 100.0);
    QRectF rectModelo(widthLayoutSpacing, heightLayoutSpacing, 50.0, 50.0);
    painter.drawRect(rectModelo);
    painter.drawText(rectModelo, Qt::AlignCenter, tr("Modelo"));

    widthLayoutSpacing += rectModelo.width();
//    QRectF rectSerie(widthLayoutSpacing, heightLayoutSpacing, 100.0, 100.0);
    QRectF rectSerie(widthLayoutSpacing, heightLayoutSpacing, 50.0, 50.0);
    painter.drawRect(rectSerie);
    painter.drawText(rectSerie, Qt::AlignCenter, tr("Serie"));

    widthLayoutSpacing += rectSerie.width();
//    QRectF rectNumero(widthLayoutSpacing, heightLayoutSpacing, 100.0, 100.0);
    QRectF rectNumero(widthLayoutSpacing, heightLayoutSpacing, 50.0, 50.0);
    painter.drawRect(rectNumero);
    painter.drawText(rectNumero, Qt::AlignCenter, tr("Número"));

    widthLayoutSpacing += rectNumero.width();
//    QRectF rectFL(widthLayoutSpacing, heightLayoutSpacing, 100.0, 100.0);
    QRectF rectFL(widthLayoutSpacing, heightLayoutSpacing, 50.0, 50.0);
    painter.drawRect(rectFL);
    painter.drawText(rectFL, Qt::AlignCenter, tr("FL"));

    widthLayoutSpacing += rectFL.width();
//    QRectF rectDataeHoraEmissao(widthLayoutSpacing, heightLayoutSpacing, 450.0, 100.0);
    QRectF rectDataeHoraEmissao(widthLayoutSpacing, heightLayoutSpacing, 278.0, 50.0);
    painter.drawRect(rectDataeHoraEmissao);
    painter.drawText(rectDataeHoraEmissao, Qt::AlignCenter, tr("Data e Hora de Emissão"));

    //Height update and widht
    heightLayoutSpacing += rectDataeHoraEmissao.height() + margem;
    widthLayoutSpacing = margem + rectTitle.width() + margem;
    //Barcode
//    QRectF rectBarcode(widthLayoutSpacing, heightLayoutSpacing, 970.0, 100.0);
    QRectF rectBarcode(widthLayoutSpacing, heightLayoutSpacing, 478.0, 50.0);
    painter.drawRect(rectBarcode);
    painter.drawText(rectBarcode, Qt::AlignCenter, tr("CONTROLE DE FISCO"));

    heightLayoutSpacing += rectBarcode.height() + margem;
//    QRectF rectProtocolo(widthLayoutSpacing, heightLayoutSpacing, 290.0, 80.0);
    QRectF rectProtocolo(widthLayoutSpacing, heightLayoutSpacing, 145.0, 40.0);
    painter.drawRect(rectProtocolo);
    painter.drawText(rectProtocolo, Qt::AlignCenter, tr("N PROTOCOLO"));

    widthLayoutSpacing += rectProtocolo.width() + margem;
//    QRectF rectSuframaDestinatario(widthLayoutSpacing, heightLayoutSpacing, 790.0, 80.0);
    QRectF rectSuframaDestinatario(widthLayoutSpacing, heightLayoutSpacing, 330.0, 40.0);
    painter.drawRect(rectSuframaDestinatario);
    painter.drawText(rectSuframaDestinatario, Qt::AlignCenter, tr("INSC. SUFRAMA DO DESTINATÁRIO"));

    /*
     * Bolco 2
     */
    heightLayoutSpacing += rectTipoDoCTE.height() + margem;
//    QRectF rectNaturezaPrestacao(1.0, heightLayoutSpacing, 2000.0, 70.0);
    QRectF rectNaturezaPrestacao(margem, heightLayoutSpacing, 993.0, 35.0);
    painter.drawRect(rectNaturezaPrestacao);
    painter.drawText(rectNaturezaPrestacao, Qt::AlignCenter, tr("CFOP - Natureza da Prestação"));

    heightLayoutSpacing += rectNaturezaPrestacao.height() + margem;
//    QRectF rectOrigemPrestacao(1.0, heightLayoutSpacing, 1000.0, 70.0);
    QRectF rectOrigemPrestacao(margem, heightLayoutSpacing, 500.0, 35.0);
    painter.drawRect(rectOrigemPrestacao);
    painter.drawText(rectOrigemPrestacao, Qt::AlignCenter, tr("Origem da Prestação"));

    widthLayoutSpacing = margem + rectOrigemPrestacao.width() + margem;
//    QRectF rectDestinoPrestacao(1.0, heightLayoutSpacing, 1000.0, 70.0);
    QRectF rectDestinoPrestacao(widthLayoutSpacing, heightLayoutSpacing, 500.0, 35.0);
    painter.drawRect(rectDestinoPrestacao);
    painter.drawText(rectDestinoPrestacao, Qt::AlignCenter, tr("Destino da Prestação"));

    heightLayoutSpacing += rectNaturezaPrestacao.height() + margem;
//    QRectF rectRemetente(1.0, heightLayoutSpacing, 1000.0, 280.0);
    QRectF rectRemetente(margem, heightLayoutSpacing, 500.0, 140.0);
    painter.drawRect(rectRemetente);
    painter.drawText(rectRemetente, Qt::AlignCenter, tr("Remetente"));

    widthLayoutSpacing = margem + rectRemetente.width() + margem;
//    QRectF rectDestinatario(1.0, heightLayoutSpacing, 1000.0, 280.0);
    QRectF rectDestinatario(widthLayoutSpacing, heightLayoutSpacing, 500.0, 140.0);
    painter.drawRect(rectDestinatario);
    painter.drawText(rectDestinatario, Qt::AlignCenter, tr("Destinatario"));

    heightLayoutSpacing += rectRemetente.height() + margem;
//    QRectF rectExpedidor_1(1.0, heightLayoutSpacing, 1000.0, 280.0);
    QRectF rectExpedidor_1(margem, heightLayoutSpacing, 500.0, 140.0);
    painter.drawRect(rectExpedidor_1);
    painter.drawText(rectExpedidor_1, Qt::AlignCenter, tr("Expeditor"));

    widthLayoutSpacing = margem + rectExpedidor_1.width() + margem;
//    QRectF rectExpeditor_2(1.0, heightLayoutSpacing, 1000.0, 280.0);
    QRectF rectExpeditor_2(widthLayoutSpacing, heightLayoutSpacing, 500.0, 140.0);
    painter.drawRect(rectExpeditor_2);
    painter.drawText(rectExpeditor_2, Qt::AlignCenter, tr("Expeditor"));

    heightLayoutSpacing += rectExpedidor_1.height() + margem;
//    QRectF rectTomador(1.0, heightLayoutSpacing, 2000.0, 70.0);
    QRectF rectTomador(margem, heightLayoutSpacing, 993.0, 35.0);
    painter.drawRect(rectTomador);
    painter.drawText(rectTomador, Qt::AlignCenter, tr("Tomador do Serviço"));

    heightLayoutSpacing += rectTomador.height() + margem;
//    QRectF RrectProdutoPredominante(1.0, heightLayoutSpacing, 1200.0, 70.0);
    QRectF rectProdutoPredominante(margem, heightLayoutSpacing, 600.0, 35.0);
    painter.drawRect(rectProdutoPredominante);
    painter.drawText(rectProdutoPredominante, Qt::AlignCenter, tr("Produto Predominante"));

    widthLayoutSpacing = rectProdutoPredominante.width() + margem;
//    QRectF RrectProdutoPredominante(1.0, heightLayoutSpacing, 400.0, 70.0);
    QRectF rectCaracteristicasCarga(widthLayoutSpacing, heightLayoutSpacing, 200.0, 35.0);
    painter.drawRect(rectCaracteristicasCarga);
    painter.drawText(rectCaracteristicasCarga, Qt::AlignCenter, tr("Características da Carga"));

}

void Danfe::painterConfiguration()
{
}
