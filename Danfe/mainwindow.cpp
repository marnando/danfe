#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtPrintSupport/QPrinter>
#include <QFileDialog>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_danfe = new Danfe;
    ui->centralWidget->layout()->addWidget(m_danfe);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Print_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Export PDF", QString(), "*.pdf");
    if (!fileName.isEmpty()) {
         if (QFileInfo(fileName).suffix().isEmpty())
         {
             fileName.append(".pdf");
         }
         QPrinter printer(QPrinter::HighResolution);
         printer.setOutputFormat(QPrinter::PdfFormat);
         printer.setPageSize(QPrinter::A4);
         printer.setOrientation(QPrinter::Portrait);
         printer.setOutputFileName(fileName);
         m_danfe->document()->print(&printer);
    }
}
