#ifndef DANFE_H
#define DANFE_H

#include <QTextEdit>
#include <QPaintEvent>
#include <QPainter>
#include <QPen>

namespace Ui {
class Danfe;
}

class Danfe : public QTextEdit
{
    Q_OBJECT

protected:
    QPainter m_painter;

public:
    explicit Danfe(QTextEdit *parent = 0);
    ~Danfe();

    void paintEvent(QPaintEvent * a_event);
    void painterConfiguration();

private:
    Ui::Danfe *ui;
};

#endif // DANFE_H
