#-------------------------------------------------
#
# Project created by QtCreator 2017-07-05T15:24:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Danfe
TEMPLATE = app
CONFIG += c++11
QT += printsupport

SOURCES += main.cpp\
        mainwindow.cpp \
    danfe.cpp

HEADERS  += mainwindow.h \
    danfe.h

FORMS    += mainwindow.ui \
    danfe.ui
